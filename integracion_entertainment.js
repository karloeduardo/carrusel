$(document).ready(function() {	
	var ellipsis = "...";
	var windowswidt = "";
	var windowheight = "";
	var column1tmp = new Array();
	var column2tmp = new Array();
	var column3tmp = new Array();
	var url = "";
	var itemstmp = new Array();
	var arrtmp = new Array();
	var cont= 0;
	var sectiontmp = "";
	var sec = "";
	var urlgsatmp = "";
	var cont2 = 1;
	var indicegral = 0;
	var swipeCount = 0;
	var constante = "";

	$(function(){
		inicializa();
	});

	function inicializa(){
		tamaniowin();
		if ($("#famosos").length){gsa_famosos();}
		if ($("#home_main").length){gsa_main();}
		if ($("#home_video").length){gsa_video();}
		if ($("#home_photo").length){gsa_foto();}
		if ($("#home_entertainment").length){gsa_entertainment();}
	}

	$(window).resize(function(){
//		inicializa();
	});

 	function tamaniowin(){
 		windowswidt  = $(window).width();
 		windowheight = $(window).height();
		constante = windowswidt / 100;
 	}

	function TrimLength(text, maxLength){
	    text = $.trim(text);
	    if (text.length > maxLength)
	    {
	        text = text.substring(0, maxLength - ellipsis.length)
	        return text.substring(0, text.lastIndexOf(" ")) + ellipsis;
	    }
	    else
	        return text;
	}

	function ordena_mozaico(section){
			arrtmp+='<section class="columnauno">';
			for (var i = 1; i < (itemstmp.length+1); i++) {
				if ((i%2)!=1){
					if (itemstmp[i]){arrtmp+=itemstmp[i];}
				}
			}
			arrtmp+='</section>';
			arrtmp+='<section class="columnados">';
			for (var j = 1; j < (itemstmp.length+1); j++) {
				if ((j%2)==1){						
					if (itemstmp[j]){arrtmp+=itemstmp[j];}
				}
			}
			arrtmp+='</section>';
		    $(section).append(arrtmp);			
			tap_mosaic_event2();	
	}

	function creacolumnas(section){
		$(section).append('<section class="columnauno"></section><section class="columnados"></section><section class="columnatres"></section>');
		ordena_mozaicoentres();
	}
	function ordena_mozaicoentres(){
		if (cont2<itemstmp.length+1){
			acomoda();			
		}
	}
	function acomoda(){
		$(sec).find(".columnauno").append(itemstmp[cont2]);
		cont2++;
		$(sec).find(".columnados").append(itemstmp[cont2]);
		cont2++;
		$(sec).find(".columnatres").append(itemstmp[cont2]);
		cont2++;
		ordena_mozaicoentres(sec);
	}
	function tap_mosaic_event(){
		var thumb = $(".pjt-thumb");
		thumb.on('mouseenter',function(){
		  $(this).addClass('active').find("img").animate({
		  	"height":"125px"
		  },50);
		  $(this).find("a").animate({
		  	"height":"100%"
		  });
		});
		thumb.on('mouseleave',function(){
		  $(this).removeClass('active').find("img").animate({
		  	"height":"100px"
		  },50);
		  $(this).find("a").animate({
		  	"height":"0"
		  });
		});
	}
	function tap_mosaic_event2(){
		var thumb = $(".pjt-thumb");
		thumb.on('mouseenter',function(){
		  $(this).addClass('active').find(".pjt-def").animate({
		  	"opacity":"1",
		  	"height":"100%"
		  },50);
		  $(this).find(".pjt-actions").animate({
		  	"width":"100%"
		  },100);
		});
		thumb.on('mouseleave',function(){
		  $(this).removeClass('active').find(".pjt-def").animate({
		  	"opacity":"0",
		  	"height":"0"
		  });
		  $(this).find(".pjt-actions").animate({
		  	"width":"0%"
		  },100);
		});
	}
	//HOME PHOTO
	function gsa_foto(){
		url = 'http://feeds.esmas.com/data-feeds-esmas/sports/master_photos.js';
		var par = 2;
		sectiontmp = "#home_photo";

		recortes = ["225","300","350"];
		var recortetmp = "";

		var urldefault = "";
		var randomimage = "";
		var recorteind = "";

		$.ajax({
			url:url,
				dataType: "jsonp",
				jsonpCallback: 'photo_items',
				url: url,
				success: function(response){
		          $(response.items).each(function(indice,objeto){
					recorteind = (Math.floor((Math.random() * recortes.length) + 1)-1);
					randomimage = (Math.floor((Math.random() * 40) + 1)-1);
					recortetmp = "http://imagemockup.com/300/"+recortes[recorteind]+"/entretenimiento/"+randomimage+"";
		          	itemstmp[cont] = '<article class="pjt-thumb notes" data-proporcion="1x1"> <figure> <img src ="'+recortetmp+'"></img><dl class="pjt-def"><dt>'+TrimLength(objeto.title,35)+'</dt><dd>'+objeto.description+'</dd><a href="'+objeto.link+'"> </a></dl></figure><figcaption class="title-thumb"><p>'+TrimLength(objeto.title,35)+'</p><div class="pjt-actions"><div class="icon1"><i></i></div><div class="icon2"><i></i></div></div></figcaption></article>';
		          	cont++;
		          });
		      }
		}).done(function(){
			ordena_mozaico(sectiontmp);
		});
	}
	// HOME VIDEO
	function gsa_video(){
		url = 'http://feeds.esmas.com/data-feeds-esmas/sports/master_videos.js';
		var par = 2;
		sectiontmp = "#home_video";
		$.ajax({
			url:url,
				dataType: "jsonp",
				jsonpCallback: 'video_items',
				url: url,
				success: function(response){
		          $(response.videos).each(function(indice,objeto){
		          	itemstmp[cont] = '<article class="pjt-thumb notes" data-proporcion="1x1"> <figure> <img src ="'+objeto.thumb+'"></img><dl class="pjt-def"><dt>'+TrimLength(objeto.title,35)+'</dt><dd>'+objeto.description+'</dd><a href="'+objeto.video_m3u8+'"> </a></dl></figure><figcaption class="title-thumb"><p>'+TrimLength(objeto.title,35)+'</p><div class="pjt-actions"><div class="icon1"><i></i></div><div class="icon2"><i></i></div></div></figcaption></article>';
		          	cont++;
		          });
		      }
		}).done(function(){
			ordena_mozaico(sectiontmp);
		});
	}
//HOME FAMOSOS
	function gsa_famosos(){
		url = 'http://feeds.esmas.com/data-feeds-esmas/sports/master_famosos.js';
		var par = 2;
		var fechanac = new Array();
		fechanac = ["17 de Enero de 1940","1 de Julio de 1980","2 de Agosto de 1988","22 de Mayo de 1968","3 de Junio de 1963"];
		var fechaind = "";
		var lugarnac = new Array();
		lugarnac = ["Ciudad de México, México","Leon Guanajuato, México","Paris Francia","Milan Italia","Tokio Japon"];
		var lugarind ="";
		$.ajax({
    		url:url,
			dataType: "jsonp",
			jsonpCallback: 'famosos',
			url: url,
			success: function(response){
              $(response.items).each(function(i,o){
				fechaind = (Math.floor((Math.random() * fechanac.length) + 1)-1); 
				lugarind = (Math.floor((Math.random() * lugarnac.length) + 1)-1); 
	            $(".pjt-main").append('<article class="pjt-thumb notes"> <figure> <img src ="'+o["images"]["300x225"]+'"></img></figure><figcaption class="title-thumb"><p><b>'+TrimLength(o.title,100)+'</b></p><p>'+fechanac[fechaind]+'<p>'+lugarnac[lugarind]+'</p></p><a href="'+o.link+'"></a><div class="pjt-actions"><div class="icon1"><i></i></div><div class="icon2"><i></i></div></div></figcaption></article>');
              });
			  tap_mosaic_event();
          }
		});			
	}
//HOME MAIN
	function gsa_main(){
		url = 'http://feeds.esmas.com/data-feeds-esmas/sports/master_mix.js';
		sec = "#home_main";
		$(sec).empty();
		var par = 2;
		var icontipotmp = "";
		var notapartmp = "";
		var tipotmp = "";
		var urlvideo1 = "http://m4vhds.tvolucion.com/i/m4v/cus/promo/5538c3f4b42730b37f9edb0ac0364bfc/2730b37f9e-,150,235,480,600,970,.mp4.csmil/master.m3u8";
		var urlvideo2 = "http://m4vhds.tvolucion.com/i/m4v/ent/promo/c1d913aa3495c6447175fc29d1a12d2b/9eebaa88d7-,150,235,480,600,970,.mp4.csmil/master.m3u8";
		var urlvideo3 = "http://m4vhds.tvolucion.com/i/m4v/cus/promo/db2cca05b9dbd4a4005b07287b7bbe52/dbd4a4005b-,150,235,480,600,970,.mp4.csmil/master.m3u8";
		var urlvideo4 = "http://m4vhds.tvolucion.com/i/m4v/boh/promo/b97d45648f840a8a36bbcf77b94341d0/840a8a36bb-,150,235,480,600,970,.mp4.csmil/master.m3u8";
		var urlvideo5 = "http://m4vhds.tvolucion.com/i/m4v/cus/promo/ef0d405e22a6ac9958cd08df0143d5bb/a6ac9958cd-,150,235,480,600,970,.mp4.csmil/master.m3u8";
		var urlsvideo = new Array();
		urlsvideo = [urlvideo1,urlvideo2,urlvideo3,urlvideo4,urlvideo5];
		var urlvidtmp = "";
		var secciontmp = new Array();
		secciontmp = ["TV","Cine","Musica","Series"];
		var tiposlement = new Array();
		tiposlement = ["foto","video","articulo"];
		var tposelemtmp = "";
		var recortes = new Array();
		recortes = ["225","300","350"];
		var recorteind = "";
		var recortetmp = "";
		var urldefault = "http://imagemockup.com/300/163/entretenimiento/1";
		var randomimage = "";

		var indtmp = "";
		$.ajax({
			url:url,
			dataType: "jsonp",
			jsonpCallback: 'mix',
			url: url,
			success: function(response){
				$(response.items).each(function(indice,objeto){
					indtmp = (Math.floor((Math.random() * secciontmp.length) + 1)-1);
					tposelemtmp =  (Math.floor((Math.random() * tiposlement.length) + 1)-1);
					urlvidtmp = (Math.floor((Math.random() * urlsvideo.length) + 1)-1);
					recorteind = (Math.floor((Math.random() * recortes.length) + 1)-1);
					randomimage = (Math.floor((Math.random() * 40) + 1)-1);

					if (tiposlement[tposelemtmp] == "video"){
						tipotmp = "tvsagui-video";
//						recortetmp = recortes[recorteind];
						recortetmp = "http://imagemockup.com/300/"+recortes[recorteind]+"/entretenimiento/"+randomimage+"";
						objeto.link = urlsvideo[urlvidtmp];
					}else if (tiposlement[tposelemtmp] == "foto"){
						tipotmp = "tvsagui-foto";
						recortetmp = "http://imagemockup.com/300/"+recortes[recorteind]+"/entretenimiento/"+randomimage+"";
					}else if (tiposlement[tposelemtmp] == "articulo"){
						tipotmp = "tvsagal-descripcion";
						recortetmp = "http://imagemockup.com/300/"+recortes[recorteind]+"/entretenimiento/"+randomimage+"";
					}
				itemstmp[cont] = '<article class="pjt-thumb notes '+notapartmp+'"> <figure> <img src ="'+recortetmp+'"></img> <dl class="pjt-def"><dt>'+objeto["title"]+'</dt><dd>'+objeto["description"]+'</dd><a href="'+objeto.link+'"> </a></dl></figure><figcaption class="title-thumb"><p>'+TrimLength(objeto.title,30)+'</p><div class="pjt-desc"><div class="icon_item"><i class="'+tipotmp+'"></i></div><div class="icon_desc"><p class="pjt-tipo">'+tiposlement[tposelemtmp]+'</p><p class="pjt-subseccion">Seccion:<b>'+secciontmp[indtmp]+'</b></p></div></div></figcaption></article>';
				cont++;
				});
			}
		}).done(function(){
			ordena_mozaico(sec);
			/*
					if (windowswidt<480){ ordena_mozaico(sec); }
			else { creacolumnas(sec); }
			*/
		});
	}

//HOME FAMOSOS
	function gsa_entertainment(){
		url = 'http://feeds.esmas.com/data-feeds-esmas/sports/master_photos.js';
		sectiontmp = "#home_entertainment";
		recortes = ["225","300","350"];
		var recortetmp = "";
		var urldefault = "";
		var randomimage = "";
		var recorteind = "";
		$.ajax({
				url:url,
				dataType: "jsonp",
				jsonpCallback: 'photo_items',
				url: url,
				success: function(response){
		          $(response.items).each(function(indice,objeto){

					randomimage = (Math.floor((Math.random() * 40) + 1)-1);

					recortetmp = "http://imagemockup.com/400/600/entretenimiento/"+randomimage+"";
					if (cont==0){
			          	itemstmp[cont]= '<article class="pjt-thumb notes pjt-notesfullscreen active"> <figure> <img src ="'+recortetmp+'"></img><div class="pjt-opacity"></div><dl class="pjt-def"><dt>'+TrimLength(objeto.title,10)+'</dt><dd>'+objeto.description+'</dd><a href="'+objeto.link+'"> </a></dl></figure><figcaption class="title-thumb"><p>'+TrimLength(objeto.title,100)+'</p><div class="pjt-actions"><div class="icon1"><i></i></div><div class="icon2"><i></i></div></div></figcaption></article>';						
					}else{
		          		itemstmp[cont]= '<article style="display:none;" class="pjt-thumb notes pjt-notesfullscreen"> <figure> <img src ="'+recortetmp+'"></img><div class="pjt-opacity"></div><dl class="pjt-def"><dt>'+TrimLength(objeto.title,10)+'</dt><dd>'+objeto.description+'</dd><a href="'+objeto.link+'"> </a></dl></figure><figcaption class="title-thumb"><p>'+TrimLength(objeto.title,100)+'</p><div class="pjt-actions"><div class="icon1"><i></i></div><div class="icon2"><i></i></div></div></figcaption></article>';
		          	}cont++;
		          });
		      }
		}).done(function(){
//		      $(sectiontmp).append(arrtmp);
			$(sectiontmp).css({
				"width" : windowswidt+"px",
				"height": windowheight+"px"
			}).animate({
				"opacity": "1"
			},100,function(){
				ordenaFullScreen(sectiontmp);					
			});
		});
	}
	function ordenaFullScreen(section){
		for (indicegral=0; indicegral <= itemstmp.length; indicegral++) {
			$(section).append(itemstmp[indicegral]);
		}
		indicegral = 0;
		$(section).swipe({
			swipeStatus:function(event, phase, direction, distance, duration, fingerCount){
				swipeCount++;
				mueveSwipe(section,indicegral,distance,direction,phase);
			},
			threshold:100,
			maxTimeThreshold:2500,
			fingers:'all',
			allowPageScroll:"vertical"
		});
	}
	
	function mueveSwipe(section,indice,distance,direction,phase){
		if (direction == "left"){
			if (indice==cont-1){
				$(section).find("article").eq(indice).css({"left":"0px"});
			}else{
				$(section).find("article").eq(indice).css({"left" : (0-distance)+"px"});
				$(section).find("article").eq(indice+1).css({
					"display":"block",
					"-ms-transform" : "scale("+((distance*constante)/1000)+")",
					"-webkit-transform" : "scale("+((distance*constante)/1000)+")",
					"transform": "scale("+((distance*constante)/1000)+")"
				}).stop().animate({
					"opacity" : "1"
				});
				if(phase == "end" || phase == "cancel"){
					$(section).find("article").eq(indice).stop().animate({"left": "-"+windowswidt+"px","z-index":"1"},function(){ $(this).css({"display":"none",}); });
					$(section).find("article").eq(indice+1).stop().css({
						"z-index" : "2",
						"-ms-transform" : "scale(1)",
						"-webkit-transform" : "scale(1)",
						"transform": "scale(1)"
					}).animate({
						"opacity" : 1
					},500,function(){
						indicegral++;
					});
				}
			}
		}
		if (direction == "right"){
			if (indice==cont-1){
				$(section).find("article").eq(indice).css({"right":"0px"});
			}else{
				$(section).find("article").eq(indice-1).css({
					"display" : "block"
				}).stop().animate({
					"left"  : "0",
					"right" : (windowswidt * -1)+distance+"px"
				});
				$(section).find("article").eq(indice).css({
					"-ms-transform" : "scale("+((distance*constante)/1000)+")",
					"-webkit-transform" : "scale("+((distance*constante)/1000)+")",
					"transform": "scale("+((distance*constante)/1000)+")"
				}).stop().animate({
					"opacity" : ((distance*constante)/1000)
				});
				if(phase == "end" || phase == "cancel"){
					$(section).find("article").eq(indice-1).stop().animate({"left":"0","right": "0px"});
					$(section).find("article").eq(indice).stop().css({
						"-ms-transform" : "scale(0.5)",
						"-webkit-transform" : "scale(0.5)",
						"transform": "scale(0.5)"
					}).animate({
						"opacity" : 0
					},500,function(){
						indicegral--;
					});
				}
			} 
		}
		if (phase=="end"||phase=="cancel"){
		}
	}


});